import re
from odoo import fields, models, api, _, exceptions


class CustomContactFields(models.Model):
    """Adding custom address
       fields"""
    _inherit = 'hr.employee'

    street_employee = fields.Char(string=' Street ', help='Street address',
                                  required='true')
    phone_employee = fields.Char(string=' Phone ',
                                 help='Phone number',
                                 required='true',
                                 readonly=False)
    city_employee = fields.Char(string=' City ', help='City',
                                required='true')
    state_employee_id = fields.Many2one(
        'res.country.state',
        string='Province',
        required='true',
        default=lambda self: self.env['res.country.state'].search(
            [('code', '=', 'NB')], limit=1),
        help='Select the state for the contact.'
    )
    country_employee_id = fields.Many2one(
        'res.country',
        string='Country',
        required='true',
        default=lambda self: self.env['res.country'].search(
            [('code', '=', 'CA')], limit=1),
        help='Select the country for the contact.'
    )
    zip_code_employee = fields.Char(string=' Zip Code ',
                                    help='Zip Code',
                                    required='true')

    category_employee = fields.Selection([
        ('full-time', 'Full-time'),
        ('part-time', 'Part-time'),
        ('supply', 'Supply')
    ], string='Employee category', required=True)

    email__employee = fields.Char(string="Email",
                                  help="Email address",
                                  readonly=False)

    @api.constrains('phone_employee', 'email__employee', 'zip_code_employee')
    def _check_employee_details(self):
        error_messages = []
        zip_code_pattern = re.compile(r'^[A-Za-z]\d[A-Za-z] \d[A-Za-z]\d$')
        email_pattern = re.compile(r'^[\w\.-]+@[\w\.-]+\.\w+$')
        phone_pattern = re.compile(r'^(\d-?){9}\d$')
        for record in self:
            if record.phone_employee and not phone_pattern.match(record.phone_employee):
                error_messages.append(_('Phone: Invalid phone number. Please enter a 10-digit phone number'))

            if record.email__employee and not email_pattern.match(record.email__employee):
                error_messages.append(_('Email: Please enter a valid email address'))

            if record.zip_code_employee and not zip_code_pattern.match(record.zip_code_employee):
                error_messages.append(_('Zip Code: enter a valid postal code in format A1A 1A1'))

        if error_messages:
            raise exceptions.ValidationError('\n'.join(error_messages))

    @api.model
    def _check_birthdays(self):
        today = fields.Date.today()
        employees = self.env["hr.employee"].search([])
        for employee in employees:
            if (
                    employee.birthday
                    and employee.birthday.day == today.day
                    and employee.birthday.month == today.month
            ):

                template_employee_id = self.env.ref('mo_hr_timesheets.'
                                                    'email_template_birthday')
                template_employee_id.send_mail(employee.id, force_send=True)
                template_coworker_id = self.env.ref('mo_hr_timesheets.'
                                                    'email_template_coworkers')

                if len(employees) > 1:
                    for coworker in employees:
                        if coworker == employee:
                            continue
                        template_coworker_id.with_context(
                            birthday_employee=employee.name
                        ).send_mail(coworker.id, force_send=True)


class CustomHrEmployeeBase(models.AbstractModel):
    _inherit = 'hr.employee.base'

    resource_calendar_id = fields.Many2one(
        'resource.calendar',
        string='Budget',
        domain="['|', ('company_id', '=', False), "
               "('company_id', '=', company_id)]"
    )
