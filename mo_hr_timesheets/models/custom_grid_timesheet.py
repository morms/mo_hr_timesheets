import logging
import datetime
from odoo import models, fields, _, api
from odoo.exceptions import ValidationError

_logger = logging.getLogger(__name__)


class CustomGridTimesheet(models.Model):
    """Adding custom views and fields."""
    _inherit = 'account.analytic.line'

    task_id = fields.Many2one(
        'project.task',
        string='Task',
        index=True,
        required=True,
        compute='_compute_task_id', store=True,
        readonly=False,
        domain="[('allow_timesheets', '=', True), "
               "('project_id', '=?', project_id)]")

    hours_per_day = 24

    def adjust_grid(self,
                    row_domain,
                    column_field,
                    column_value,
                    cell_field,
                    change):
        if column_field != 'date' or cell_field != 'unit_amount':
            return False

        date_str = column_value.split('/')[0]
        try:
            date = datetime.datetime.strptime(date_str,
                                              "%Y-%m-%d").date()
        except ValueError:
            return False

        project_id, task_id = None, None
        for domain_part in row_domain:
            if isinstance(domain_part, (list, tuple)):
                if domain_part[0] == 'project_id' and domain_part[1] == '=':
                    project_id = domain_part[2]
                elif domain_part[0] == 'task_id' and domain_part[1] == '=':
                    task_id = domain_part[2]

        if not project_id:
            raise ValidationError(_("Project is required."))

        try:
            change = float(change)
        except ValueError:
            raise ValidationError(_("Change must "
                                    "be a valid number."))

        domain = [('date', '=', date)]
        total_hours_for_day = sum(self.
                                  search(domain).
                                  mapped('unit_'
                                         'amount'))

        if task_id:
            existing_hours_for_task = sum(
                self.search([('date', '=', date),
                             ('task_id', '=', task_id)]).mapped('unit_amount'))
            total_hours_for_day = (total_hours_for_day -
                                   existing_hours_for_task +
                                   change)
        else:
            total_hours_for_day += change

        hours_per_day = 24

        if total_hours_for_day > hours_per_day:
            raise ValidationError(_("Total hours for date %(date)s "
                                    "cannot exceed 24 "
                                    "hours. Total now: "
                                    "%(hours).2f "
                                    "hours.") %
                                  {'date': date_str,
                                   'hours': total_hours_for_day})

        domain = [('date', '=', date), ('project_id', '=', project_id)]
        if task_id:
            domain.append(('task_id', '=', task_id))

        existing_record = self.search(domain, limit=1)

        try:
            if existing_record:
                existing_record.unit_amount = change
            else:
                self.create({
                    'date': date,
                    'project_id': project_id,
                    'unit_amount': change,
                    'task_id': task_id if task_id else False
                })
        except Exception as e:
            _logger.error("Error in writing or "
                          "creating record: %s", e)
            return False

        return True

    def _check_total_hours(self,
                           date,
                           unit_amount,
                           additional_hours=0):
        if not date:
            return

        domain = [('date', '=', date), ('user_id', '=', self.env.user.id)]

        existing_hours = sum(self.search(domain).mapped('unit_amount'))
        total_hours = existing_hours + unit_amount + additional_hours

        hours_per_day = 24

        if total_hours > hours_per_day:
            raise ValidationError(
                _("Total hours for date %(date)s cannot exceed 24 hours."
                  " You have already logged"
                  " %(existing_hours)s hours, and the current "
                  "operation would increase "
                  "this to %(total_hours)s hours.") % {
                    'date': date,
                    'existing_hours': existing_hours,
                    'total_hours': total_hours
                }
            )

    def _check_existing_task_entry(self, task_id, date):
        if task_id and date:
            existing_entries = self.search([
                ('task_id', '=', task_id),
                ('date', '=', date),
            ])
            if existing_entries:
                raise ValidationError(
                    _("A timesheet entry for this task"
                      " on the specified date already exists. "
                      "Please update the "
                      "existing entry instead of creating a new one."))

    @api.model
    def create(self, vals):
        self._check_existing_task_entry(vals.get('task_id'),
                                        vals.get('date'))

        self._check_total_hours(vals.get('date'),
                                vals.get('unit_amount'))

        return super(CustomGridTimesheet, self).create(vals)

    def write(self, vals):
        if 'task_id' in vals or 'date' in vals:
            for record in self:
                new_task_id = vals.get('task_id', record.task_id.id)
                new_date = vals.get('date', record.date)
                if new_task_id != record.task_id.id or new_date != record.date:
                    self._check_existing_task_entry(new_task_id, new_date)

        if 'date' in vals or 'unit_amount' in vals:
            for record in self:
                new_date = vals.get('date', record.date)
                additional_hours = (vals.get('unit_amount',
                                             record.unit_amount) -
                                    record.unit_amount) if ('unit_amount' in
                                                            vals) else 0
                self._check_total_hours(new_date, 0,
                                        additional_hours=additional_hours)

        return super(CustomGridTimesheet, self).write(vals)
