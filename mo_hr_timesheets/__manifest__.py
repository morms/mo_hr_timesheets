{
    'name': "Custom HR timesheet",
    'summary': "Custom HR timesheet and Employee Information",
    'version': '16.0.1.0.0',
    'website': "https://mcaf.nb.ca/en/",
    'author': "MCAF",
    'category': "HR",
    'license': 'OPL-1',
    "application": True,
    "installable": True,
    'depends': ['base', 'om_hr_payroll', 'project', 'generic_grid'],
    'data': [
        'security/ir.model.access.csv',
        'data/kron_data.xml',
        'views/hr_employee_views.xml',
        'views/hr_custom_grid_timesheet_views.xml',
        'views/templates.xml',
        'data/employee_birthday_template.xml',
        'data/employee_90_days_confirm_template.xml',
        'data/employee_end_probation.xml',
        'data/employee_reminder_probation.xml',
    ],
    'assets': {
        'web.assets_backend': [
            'mo_hr_timesheets/static/src/js/grid_controller.js',
        ],

    },
}
