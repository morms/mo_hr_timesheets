odoo.define('mo_hr_timesheets.CustomGridControllers', function (require) {
    "use strict";

    var GridController = require('generic_grid.GridController');
    var GridView = require('generic_grid.GridView');
    var registry = require('@web/core/registry');
    var AbstractController = require('web.AbstractController');
    var config = require('web.config');
    var core = require('web.core');
    var dialogs = require('web.view_dialogs');
    var utils = require('web.utils');
    var concurrency = require('web.concurrency');
    var qweb = core.qweb;
    var _t = core._t;

    GridController.include({

    /**
 * Triggers a success notification indicating that hours have been successfully posted.
 * This non-sticky notification informs the user of successful hour posting operations.
 */

        notifyHoursPosted: function() {
            this.trigger_up('do_action', {
                action: {
                    type: 'ir.actions.client',
                    tag: 'display_notification',
                    params: {
                        'message': 'Hours have been posted successfully',
                        'type': 'success',
                        'sticky': false,
                    }
                }
            });
        },

        _onCellEdited: function(event) {
            var state = this.model.get();
            this._grid_adjust({
                row: utils.into(state, event.data.row_path),
                col: utils.into(state, event.data.col_path),
                value: utils.into(state, event.data.cell_path).value,
            }, event.data.value).then(() => {
                this.notifyHoursPosted(); //After adjusting the cell value, it triggers a notification
                if (event.data.doneCallback !== undefined) {
                    event.data.doneCallback();
                }
            }).guardedCatch(() => {
                if (event.data.doneCallback !== undefined) {
                    event.data.doneCallback();
                }
            });
        },

        _grid_adjust: function(cell, newValue) {
            var difference = newValue;
            if (Math.abs(difference) < 1e-6) {
                return Promise.resolve();
            }
            var gridState = this.model.get();
            var domain = this.model.domain.concat(cell.row.domain);
            var self = this;
            return this.mutex.exec(function() {
                return self._rpc({
                    model: self.modelName,
                    method: self.adjustName,
                    args: [
                        [], domain, gridState.colField,
                        cell.col.values[gridState.colField][0],
                        gridState.cellField, difference,
                    ],
                    context: self.model.getContext(),
                }).then(function() {
                    return self.model.reload();
                }).then(function() {
                    var newState = self.model.get();
                    return self.renderer.updateState(newState, {});
                }).then(function() {
                    self._updateButtons(gridState);
                });
            });
        },
    });
});
